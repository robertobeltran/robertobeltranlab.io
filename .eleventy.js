module.exports = function(eleventyConfig) {
    eleventyConfig.setTemplateFormats([
        "css",
        "png",
        "jpg",
        "webp",
        "woff2",
        "otf",
        "ttf",
        "html",
        "liquid",
        "jpeg",
        "svg",
        "ico"
    ]);
    eleventyConfig.setLiquidOptions({
        dynamicPartials: true
    });
};